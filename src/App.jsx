import "./App.scss";
import AppProvider from "./components/store/AppProvider";
import Header from "./components/Header/Header";
import Home from "./components/Home/Home";
import About from "./components/About/About";
import Skills from "./components/Skills/Skills";
import Portfolio from "./components/Portfolio/Portfolio";
import Contact from "./components/Contact/Contact";
import Footer from "./components/Footer/Footer";
import ToTop from "./components/ToTop/ToTop";
import ToastMessage from "./components/UI/ToastMessage";

function App() {

  return (
    <AppProvider>
      <ToastMessage />
      <div className="App grid">
        <Header/>
        <div className="wide">
          <Home />
          <About />
          <Skills />
          <Portfolio />
          <Contact />
          <ToTop />
        </div>
        <Footer />
      </div>
    </AppProvider>
  );
}

export default App;
