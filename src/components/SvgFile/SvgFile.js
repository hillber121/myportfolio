import { ReactComponent as NavLIcon } from "./../../assets/icons/nav_left.svg";
import { ReactComponent as NavRIcon } from "./../../assets/icons/nav_right.svg";
import { ReactComponent as ArrowRIcon } from "./../../assets/icons/arrow_right.svg";
import { ReactComponent as CalendarIcon } from "./../../assets/icons/calendar.svg";
import { ReactComponent as PhoneIcon } from "./../../assets/icons/phone.svg";
import { ReactComponent as EmailIcon } from "./../../assets/icons/email.svg";
import { ReactComponent as LocationIcon } from "./../../assets/icons/location.svg";
import { ReactComponent as SendIcon } from "./../../assets/icons/send.svg";
import { ReactComponent as MoonIcon } from "./../../assets/icons/moon.svg";
import { ReactComponent as SunIcon } from "./../../assets/icons/sun.svg";
import { ReactComponent as LinkedInIcon } from "./../../assets/icons/linkedin.svg";
import { ReactComponent as TelegramIcon } from "./../../assets/icons/telegram.svg";
import { ReactComponent as GitlabIcon } from "./../../assets/icons/gitlab.svg";
import { ReactComponent as LinkBtnIcon } from "./../../assets/icons/link_btn.svg";
import { ReactComponent as GitlabBtnIcon } from "./../../assets/icons/gitlab_btn.svg";
import { ReactComponent as FigmaBtnIcon } from "./../../assets/icons/figma_btn.svg";
import { ReactComponent as MenuIcon } from "./../../assets/icons/mobile_header/menu.svg";
import { ReactComponent as CloseIcon } from "./../../assets/icons/mobile_header/close.svg";
import { ReactComponent as HomeIcon } from "./../../assets/icons/mobile_header/home.svg";
import { ReactComponent as ContactIcon } from "./../../assets/icons/mobile_header/send_line.svg";
import { ReactComponent as PortfolioIcon } from "./../../assets/icons/mobile_header/image.svg";
import { ReactComponent as SkillsIcon } from "./../../assets/icons/mobile_header/list.svg";
import { ReactComponent as AboutIcon } from "./../../assets/icons/mobile_header/user.svg";
import { ReactComponent as SoftSkillsIcon } from "./../../assets/icons/mind_soft.svg";
import { ReactComponent as TechSkillsIcon } from "./../../assets/icons/mind_tech.svg";
import { ReactComponent as HtmlIcon } from "./../../assets/icons/skills/html.svg";
import { ReactComponent as CssIcon } from "./../../assets/icons/skills/css.svg";
import { ReactComponent as SassIcon } from "./../../assets/icons/skills/sass.svg";
import { ReactComponent as JsIcon } from "./../../assets/icons/skills/js.svg";
import { ReactComponent as ReactIcon } from "./../../assets/icons/skills/react.svg";
import { ReactComponent as FigmaIcon } from "./../../assets/icons/skills/figma.svg";
import { ReactComponent as AttitudeIcon } from "./../../assets/icons/skills/attitude.svg";
import { ReactComponent as CommuIcon } from "./../../assets/icons/skills/communication.svg";
import { ReactComponent as OrganIcon } from "./../../assets/icons/skills/organization.svg";
import { ReactComponent as ProblemIcon } from "./../../assets/icons/skills/problem.svg";
import { ReactComponent as TeamworkIcon } from "./../../assets/icons/skills/teamwork.svg";
import { ReactComponent as EnglishIcon } from "./../../assets/icons/skills/english.svg";
import { ReactComponent as ArrowTIcon } from "./../../assets/icons/arrow_top.svg";

export {
  NavLIcon,
  NavRIcon,
  ArrowRIcon,
  CalendarIcon,
  PhoneIcon,
  EmailIcon,
  LocationIcon,
  SendIcon,
  MoonIcon,
  SunIcon,
  LinkedInIcon,
  TelegramIcon,
  GitlabIcon,
  LinkBtnIcon,
  GitlabBtnIcon,
  FigmaBtnIcon,
  MenuIcon,
  CloseIcon,
  HomeIcon,
  ContactIcon,
  PortfolioIcon,
  SkillsIcon,
  AboutIcon,
  SoftSkillsIcon,
  TechSkillsIcon,
  HtmlIcon,
  CssIcon,
  SassIcon,
  JsIcon,
  ReactIcon,
  FigmaIcon,
  AttitudeIcon,
  CommuIcon,
  OrganIcon,
  ProblemIcon,
  TeamworkIcon,
  EnglishIcon,
  ArrowTIcon,
};
