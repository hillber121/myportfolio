import "./_button.scss";

function Button(props) {
  return (
    <button
      type={props.type || "button"}
      defaultChecked={props.defaultChecked}
      className={`Button ${props.className || ""} ${
        props.isSmall ? "Button--small" : "Button--normal"
      }`}
      onClick={props.onClick}
    >
      <div>{props.children}</div>
      {/* render icon component when exists */}
      {props.icon && (
        <div
          className={`Button__icon ${
            props.leftIcon ? "Button__icon--left" : "" // icon will be placed on the right by default
          }`}
        >
          {props.icon}
        </div>
      )}
    </button>
  );
}

export default Button;
