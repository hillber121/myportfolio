import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./_toastMessage.scss";
import { CloseIcon } from "../SvgFile/SvgFile";

const CloseButton = ({ closeToast }) => (
  <CloseIcon className="ToastMessage__close" onClick={closeToast} />
);

export default function ToastMessage() {
  return (
    <>
      <ToastContainer
        toastClassName="ToastMessage"
        position="top-right"
        autoClose={3000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        closeButton={CloseButton}
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
    </>
  );
}
