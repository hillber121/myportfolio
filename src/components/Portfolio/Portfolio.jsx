import { useRef, useMemo, useCallback } from "react";
// core version + navigation, pagination modules:
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Pagination } from "swiper";

import "./_portfolio.scss";
import Button from "../UI/Button";
import {
  NavLIcon,
  NavRIcon,
  LinkBtnIcon,
  GitlabBtnIcon,
  FigmaBtnIcon,
} from "./../SvgFile/SvgFile";
import port1Img from "./../../assets/images/portfolio1.png";
import port2Img from "./../../assets/images/portfolio2.png";
import port3Img from "./../../assets/images/portfolio3.png";
// Import Swiper styles
import "swiper/scss";
import "swiper/scss/pagination";

function Portfolio() {
  // Carousel configuration
  const prevRef = useRef(null);
  const nextRef = useRef(null);
  const swiper = useMemo(
    () => ({
      containerModifierClass: "Portfolio__slider-",
      modules: [Navigation, Pagination],
      onInit: (swiper) => {
        swiper.params.navigation.prevEl = prevRef.current;
        swiper.params.navigation.nextEl = nextRef.current;
        swiper.navigation.init();
        swiper.navigation.update();
      },
      navigation: {
        // Both prevEl & nextEl are null at render so this does not work
        prevEl: prevRef.current,
        nextEl: nextRef.current,
        disabledClass: "Portfolio__work-btn--disabled",
      },
      pagination: {
        clickable: true,
        modifierClass: "Portfolio__work-pagination-", // Portfolio__work-pagination-bullets
        bulletClass: "Portfolio__work-pagination-bullet",
        bulletActiveClass: "Portfolio__work-pagination-bullet-active",
      },
      slidesPerView: "auto",
      speed: 400,
      className: "Portfolio__work-swiper",
    }),
    []
  );

  // Click button to open link handle
  const btnCompRender = useCallback((links) => {
    return (
      <div className="Portfolio__work-tab-content-btns">
        <a href={links.web} title={links.web} target="_blank" rel="noopener noreferrer">
          <Button
            isSmall
            icon={<LinkBtnIcon />}
            className="Portfolio__work-tab-content-btns-btn"
          >
            Demo
          </Button>
        </a>
        <a href={links.gitlab} title={links.gitlab} target="_blank" rel="noopener noreferrer">
          <GitlabBtnIcon className="Portfolio__work-tab-content-btns-link" />
        </a>
        {links.figma && (
          <a href={links.figma} title={links.figma} target="_blank" rel="noopener noreferrer">
            <FigmaBtnIcon className="Portfolio__work-tab-content-btns-link" />
          </a>
        )}
      </div>
    );
  }, []);

  const port1BtnComp = useMemo(
    () =>
      btnCompRender({
        web: "https://nhabong-6b68c.web.app/",
        gitlab: "https://gitlab.com/hillber121/ecomWebApp",
        figma:
          "https://www.figma.com/file/E0MjWnEJWiPUYOfPdDWgx2/Ecom_Nha-Bong.fig..fig.?node-id=30%3A182&t=i4sDNhGYZPtFyNEy-1",
      }),
    [btnCompRender]
  );

  const port2BtnComp = useMemo(
    () =>
      btnCompRender({
        web: "https://food-order-nguyendaihung.web.app/",
        gitlab: "https://gitlab.com/hillber121/foodorderapp",
      }),
    [btnCompRender]
  );

  const port3BtnComp = useMemo(
    () =>
      btnCompRender({
        web: "https://portfolio-nguyendaihung.web.app/",
        gitlab: "https://gitlab.com/hillber121/myportfolio",
        figma:
          "https://www.figma.com/file/eGQwIsD6y3bH2yAWvYcghS/Portfolio?node-id=379%3A2699&t=hQptd9mtgcSk9vjZ-1",
      }),
    [btnCompRender]
  );

  return (
    <section className="Portfolio" id="portfolio">
      <div className="section row">
        <div className="text-section-title">Portfolio</div>
        <div className="text-section-subtitle">Most recent work</div>
      </div>
      {/* Main content - carousel */}
      <div className="row">
        <div className="Portfolio__work">
          <Swiper {...swiper}>
            {/* Project 1 */}
            <SwiperSlide className="Portfolio__work-tab">
              <div className="Portfolio__work-tab-img">
                <img src={port1Img} alt="Portfolio 1" />
              </div>
              <div className="Portfolio__work-tab-content">
                <div className="Portfolio__work-tab-content-text">
                  <div className="text-body-title">
                    Ecom web page (Implementing)
                  </div>
                  <div>Nhà Bông - soft toys and wax flower shop.</div>
                </div>
                {port1BtnComp}
              </div>
            </SwiperSlide>
            {/* Project 2 */}
            <SwiperSlide className="Portfolio__work-tab">
              <div className="Portfolio__work-tab-img">
                <img src={port2Img} alt="Portfolio 2" />
              </div>
              <div className="Portfolio__work-tab-content">
                <div className="Portfolio__work-tab-content-text">
                  <div className="text-body-title">Food order</div>
                  <div>Food ordering app.</div>
                </div>
                {port2BtnComp}
              </div>
            </SwiperSlide>
            {/* Project 3 */}
            <SwiperSlide className="Portfolio__work-tab">
              <div className="Portfolio__work-tab-img">
                <img src={port3Img} alt="Portfolio 3" />
              </div>
              <div className="Portfolio__work-tab-content">
                <div className="Portfolio__work-tab-content-text">
                  <div className="text-body-title">
                    Portfolio - Nguyen Dai Hung
                  </div>
                  <div>My portfolio.</div>
                </div>
                {port3BtnComp}
              </div>
            </SwiperSlide>
            <div ref={prevRef} className="Portfolio__work-btn--prev">
              <NavLIcon />
            </div>
            <div ref={nextRef} className="Portfolio__work-btn--nxt">
              <NavRIcon />
            </div>
          </Swiper>
        </div>
      </div>
    </section>
  );
}

export default Portfolio;
