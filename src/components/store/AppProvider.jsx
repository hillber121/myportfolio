import { createContext, useState, useEffect } from "react";

export const AppContext = createContext({});

function AppProvider(props) {
  // Notice scroll
  const [isScrolled, setIsScrolled] = useState(window.scrollY !== 0);
  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY !== 0) {
        setIsScrolled(true);
      } else setIsScrolled(false);
    };
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  // Scroll Attributes
  const scrollAtts = {
    activeClass: "Header__nav--active",
    smooth: true,
    spy: true,
    offset: -200,
    duration: 500,
  };

  // Check mobile responsive
  const [isMobile, setIsMobile] = useState(window.innerWidth < 674);
  useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth < 674) {
        setIsMobile(true);
      }
      if (window.innerWidth >= 674) {
        setIsMobile(false);
      }
    };
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, [isMobile]);

  // Show menu for mobile only
  const [isShowed, setIsShowed] = useState(false);
  const showMenuHandler = () => {
    setIsShowed(true);
  };
  const closeMenuHandler = () => {
    setIsShowed(false);
  };

  // Transition configuration
  const duration = 300;
  const defaultStyles = {
    transition: `all ${duration}ms ease-in-out`,
    opacity: 0,
  };
  const opaStyles = {
    entering: { opacity: 1 },
    entered: { opacity: 1 },
    exiting: { opacity: 0 },
    exited: { opacity: 0 },
  };
  const slideUpStyles = {
    entering: { top: "-185px" },
    entered: { top: "-185px" },
    exiting: { top: 0 },
    exited: { top: 0 },
  };

  // Declare variable to store data
  const appContext = {
    isScrolled,
    isMobile,
    scrollAtts,
    isShowed,
    showMenuHandler,
    closeMenuHandler,
    transitionConfig: {
      duration,
      defaultStyles,
      opaStyles,
      slideUpStyles,
    },
  };

  return (
    <AppContext.Provider value={appContext}>
      {props.children}
    </AppContext.Provider>
  );
}

export default AppProvider;
