import "./_about.scss";

import { CalendarIcon } from "./../SvgFile/SvgFile";

function About() {
  return (
    <section className="About" id="about">
      <div className="section row">
        <div className="text-section-title">About</div>
        <div className="text-section-subtitle">Experience</div>
      </div>
      <div className="About__exp row">
        <div className="About__exp-container">
          {/* Experience 1 */}
          <div className="About__exp-data">
            <div className="About__exp-data-tab">
              <div className="text-body-title">
                Self-study Front-end developer
              </div>
              <div>Home</div>
              <div className="About__exp-data-tab-time">
                <CalendarIcon className="About__exp-data-tab-time-icon" />
                <div className="About__exp-data-tab-time-text">
                  Jun 2022 - Now
                </div>
              </div>
              <div className="About__exp-data-tab-desc">
                <p>Self learning Javascript, HTML, CSS, ReactJS, SCSS.</p>
                <p>
                  Projects: Nhà Bông Ecom Shop; Food Order App; My Portfolio
                </p>
              </div>
            </div>
            <div className="About__exp-data-sep">
              <div className="About__exp-data-sep-ellipse" />
              <div className="About__exp-data-sep-line"></div>
            </div>
            <div className="About__exp-data-blank"></div>
          </div>

          {/* Experience 2 */}
          <div className="About__exp-data">
            <div className="About__exp-data-blank"></div>
            <div className="About__exp-data-sep">
              <div className="About__exp-data-sep-ellipse" />
              <div className="About__exp-data-sep-line"></div>
            </div>
            <div className="About__exp-data-tab">
              <div className="text-body-title">Software developer</div>
              <div>Renesas Design Vietnam Co., Ltd</div>
              <div className="About__exp-data-tab-time">
                <CalendarIcon className="About__exp-data-tab-time-icon" />
                <div className="About__exp-data-tab-time-text">
                  Nov 2020 - Jun 2022
                </div>
              </div>
              <div className="About__exp-data-tab-desc">
                <p>
                  Developing and maintaining for SoC software - MCAL digital
                  input/output driver.
                </p>
                <p>Scheduling tasks for team members.</p>
                <p>Handling integration test and unit test.</p>
              </div>
            </div>
          </div>

          {/* Experience 3 */}
          <div className="About__exp-data">
            <div className="About__exp-data-tab">
              <div className="text-body-title">Software developer</div>
              <div>Robert Bosch Vietnam Co., ltd</div>
              <div className="About__exp-data-tab-time">
                <CalendarIcon className="About__exp-data-tab-time-icon" />
                <div className="About__exp-data-tab-time-text">
                  May 2019 - Aug 2020
                </div>
              </div>
              <div className="About__exp-data-tab-desc">
                <p>
                  Analysing requirements related to vehicle functionality from
                  OEMs.
                </p>
                <p>
                  Handling the design, code and testing applies ASPICE process
                  and V model.
                </p>
              </div>
            </div>
            <div className="About__exp-data-sep">
              <div className="About__exp-data-sep-ellipse" />
              <div className="About__exp-data-sep-line"></div>
            </div>
            <div className="About__exp-data-blank"></div>
          </div>

          {/* Experience 4 */}
          <div className="About__exp-data">
            <div className="About__exp-data-blank"></div>
            <div className="About__exp-data-sep">
              <div className="About__exp-data-sep-ellipse" />
              <div className="About__exp-data-sep-line"></div>
              <div className="About__exp-data-sep-ellipse" />
            </div>
            <div className="About__exp-data-tab">
              <div className="text-body-title">Mechatronics engineer</div>
              <div>HCMC University of technology and education</div>
              <div className="About__exp-data-tab-time">
                <CalendarIcon className="About__exp-data-tab-time-icon" />
                <div className="About__exp-data-tab-time-text">
                  Sep 2015 - Nov 2019
                </div>
              </div>
              <div className="About__exp-data-tab-desc">
                <p>Major: Mechatronics engineering</p>
                <p>GPA: 7.23</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default About;
