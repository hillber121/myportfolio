import { useContext } from "react";
import { Link } from "react-scroll";

import "./_home.scss";
import { AppContext } from "../store/AppProvider";
import Button from "../UI/Button";
import avatarImg from "./../../assets/images/avatar.jpg";
import { SendIcon } from "./../SvgFile/SvgFile";

function Home() {
  const scrollAtts = useContext(AppContext).scrollAtts;

  return (
    <section className="Home row" id="home">
      <div className="Home__content">
        <h1 className="Home__content-title">I'm Hung</h1>
        <h3 className="Home__content-subtitle">Front-end Developer</h3>
        <p className="Home__content-objectives">
          With 3-year experience in embedded systems field, it is a challenge
          for me when turning into a web developer but it also brought me many
          strengths - being familiar with coding, project handling, problem
          solving and team working. I am aiming to become a good front-end
          developer.
        </p>
        <Link {...scrollAtts} to="contact">
          <Button icon={<SendIcon />}>Contact me</Button>
        </Link>
      </div>
      <div className="Home__space" />
      <div className="Home__avatar">
        <img src={avatarImg} alt="avatar" />
      </div>
    </section>
  );
}

export default Home;
