import { useForm } from "react-hook-form";
import { ErrorMessage } from "@hookform/error-message";
import emailjs from "@emailjs/browser";
import { toast } from "react-toastify";

import "./_contact.scss";
import Button from "../UI/Button";
import {
  PhoneIcon,
  EmailIcon,
  LocationIcon,
  SendIcon,
} from "./../SvgFile/SvgFile";

function Contact() {
  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm();

  const onSubmit = (data) => {
    // Template configuration
    const templateParams = {
      to_name: "Nguyen Dai Hung",
      from_name: data.nameInp,
      from_email: data.emailInp,
      message: data.messageInp,
    };

    const sendingMessage = () =>
      emailjs
        .send(
          "service_c5oszzy",
          "hung_portfolio_contact",
          templateParams,
          "pK8jWALizYpL739gh"
        )
        .then((result) => {
          console.log(result.text);
        })
        .catch((error) => {
          throw new Error(error);
        });
    toast.promise(sendingMessage, {
      pending: "Sending message",
      success: "Send message successfully!",
      error: "Send message failed!",
    });
  };

  // Input components generation
  const inpComGen = (inpId) => {
    // Rule: inpId must has suffix "Inp"
    // Input's string
    const inpStr = inpId.slice(0, -3);
    // Check if input is a message
    const isMessage = inpStr.indexOf("message") !== -1;
    // Check if input is an email
    const isEmail = inpStr.indexOf("email") !== -1;
    // Input's attributes
    const inpAtts = {
      id: inpId,
      ...register(inpId, {
        required: `Please enter your ${inpStr}`,
        minLength: {
          value: 2,
          message: `Invalid ${inpStr}`,
        },
        pattern: isEmail
          ? {
              value: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
              message: `Invalid ${inpStr}`,
            }
          : null,
        validate: (val) => val.trim() !== "" || `Invalid ${inpStr}`,
      }),
    };
    // Input's Class
    const inpClass = `Contact__form-field ${
      isMessage ? "Contact__form-field-message" : ""
    } ${errors[inpId] ? "Contact__form-field-err" : ""}`;

    return (
      <div className={inpClass}>
        <label htmlFor={inpId}>
          {inpStr.charAt(0).toUpperCase() + inpStr.slice(1)}
        </label>
        {isMessage ? <textarea {...inpAtts} /> : <input {...inpAtts} />}
        <ErrorMessage
          className="Contact__form-field-err-msg"
          errors={errors}
          name={inpId}
          as="div"
        />
      </div>
    );
  };

  return (
    <section className="Contact" id="contact">
      <div className="section row">
        <div className="text-section-title">Contact me</div>
        <div className="text-section-subtitle">Get in touch</div>
      </div>
      <div className="row">
        <div className="Contact__info-container">
          <div className="Contact__info">
            <PhoneIcon className="Contact__info-icon" />
            <div>
              <div className="text-body-title">Phone</div>
              <div>0909872305</div>
            </div>
            <EmailIcon className="Contact__info-icon" />
            <div>
              <div className="text-body-title">Email</div>
              <div>hillber121@gmail.com</div>
            </div>
            <LocationIcon className="Contact__info-icon" />
            <div>
              <div className="text-body-title">Location</div>
              <div>
                Ta Quang Buu street, ward 6, district 8, Ho Chi Minh city
              </div>
            </div>
          </div>
        </div>
        <div className="Contact__form-container">
          <form onSubmit={handleSubmit(onSubmit)} className="Contact__form">
            {/* Name input */}
            {inpComGen("nameInp")}
            {/* Email input */}
            {inpComGen("emailInp")}
            {/* Message input */}
            {inpComGen("messageInp")}
            <div>
              <Button type="submit" icon={<SendIcon />}>
                Send message
              </Button>
            </div>
          </form>
        </div>
      </div>
    </section>
  );
}

export default Contact;
