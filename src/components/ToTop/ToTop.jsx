import { useContext } from "react";
import { animateScroll } from "react-scroll";
import { Transition } from "react-transition-group";

import { AppContext } from "../store/AppProvider";
import "./_toTop.scss";
import { ArrowTIcon } from "../SvgFile/SvgFile";

function ToTop() {
  const appCtx = useContext(AppContext);

  // Handle scroll to top
  const scrollToTopHandler = () => {
    animateScroll.scrollToTop({ duration: 300 });
  };

  // Transition
  const { duration, defaultStyle, opaStyles } = appCtx.transitionConfig;
  return (
    <Transition
      in={appCtx.isScrolled}
      timeout={duration}
      mountOnEnter
      unmountOnExit
    >
      {(state) => (
        <div
          className="ToTop"
          onClick={scrollToTopHandler}
          style={{
            ...defaultStyle,
            ...opaStyles[state],
          }}
        >
          <ArrowTIcon />
        </div>
      )}
    </Transition>
  );
}

export default ToTop;
