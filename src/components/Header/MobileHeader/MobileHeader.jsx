import { useContext } from "react";
import { Link } from "react-scroll";
import { Transition } from "react-transition-group";

import "./_mobileHeader.scss";
import { AppContext } from "../../store/AppProvider";
import DarkModeToggle from "../../DarkModeToggle/DarkModeToggle";
import {
  MenuIcon,
  CloseIcon,
  HomeIcon,
  ContactIcon,
  PortfolioIcon,
  SkillsIcon,
  AboutIcon,
} from "./../../SvgFile/SvgFile";

function MobileHeader() {
  const appCtx = useContext(AppContext);

  // Transition
  const { duration, defaultStyles, opaStyles, slideUpStyles } =
    appCtx.transitionConfig;

  return (
    <div className="MobileHeader">
      <Transition
        in={appCtx.isShowed}
        timeout={duration}
        mountOnEnter
        unmountOnExit
      >
        {(state) => (
          <ul
            className="MobileHeader__nav"
            style={{
              ...defaultStyles,
              ...opaStyles[state],
              ...slideUpStyles[state],
            }}
          >
            <li>
              <Link {...appCtx.scrollAtts} to="home">
                <HomeIcon />
                Home
              </Link>
            </li>
            <li>
              <Link {...appCtx.scrollAtts} to="about">
                <AboutIcon />
                About
              </Link>
            </li>
            <li>
              <Link {...appCtx.scrollAtts} to="skills">
                <SkillsIcon />
                Skills
              </Link>
            </li>
            <li>
              <Link {...appCtx.scrollAtts} to="portfolio">
                <PortfolioIcon />
                Portfolio
              </Link>
            </li>
            <li>
              <Link {...appCtx.scrollAtts} to="contact">
                <ContactIcon />
                Contact me
              </Link>
            </li>
          </ul>
        )}
      </Transition>
      <div className="MobileHeader__btn">
        <DarkModeToggle />
        {!appCtx.isShowed ? (
          <MenuIcon
            className="MobileHeader__btn-icon"
            onClick={appCtx.showMenuHandler}
          />
        ) : (
          <CloseIcon
            className="MobileHeader__btn-icon"
            onClick={appCtx.closeMenuHandler}
          />
        )}
      </div>
    </div>
  );
}

export default MobileHeader;
