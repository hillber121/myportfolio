import { useContext } from "react";
import { Transition } from "react-transition-group";

import "./_header.scss";
import { AppContext } from "../store/AppProvider";
import WebTabletHeader from "./WebTabletHeader/WebTabletHeader";
import MobileHeader from "./MobileHeader/MobileHeader";

function Header() {
  const appCtx = useContext(AppContext);

  // Transition
  const { duration, defaultStyles, opaStyles } = appCtx.transitionConfig;

  // Header class
  const headerClass = `Header ${appCtx.isScrolled ? "Header--shadow" : ""}`;

  return (
    <>
      <Transition
        in={appCtx.isMobile && appCtx.isShowed}
        timeout={duration}
        mountOnEnter
        unmountOnExit
      >
        {(state) => (
          <div
            className="backdrop"
            onClick={appCtx.closeMenuHandler}
            style={{
              ...defaultStyles,
              ...opaStyles[state],
            }}
          />
        )}
      </Transition>
      <header className={headerClass}>
        <div className="Header__container wide">
          <div className="Header__name">Nguyen Dai Hung</div>
          {appCtx.isMobile ? <MobileHeader /> : <WebTabletHeader />}
        </div>
      </header>
    </>
  );
}

export default Header;
