import { useContext } from "react";
import { Link } from "react-scroll";

import "./_webTabletHeader.scss";
import { AppContext } from "../../store/AppProvider";
import DarkModeToggle from "../../DarkModeToggle/DarkModeToggle";

function WebTabletHeader() {
  const scrollAtts = useContext(AppContext).scrollAtts;

  return (
    <ul className="WebTabletHeader">
      <li>
        <Link {...scrollAtts} to="about">
          About
        </Link>
      </li>
      <li>
        <Link {...scrollAtts} to="skills">
          Skills
        </Link>
      </li>
      <li>
        <Link {...scrollAtts} to="portfolio">
          Portfolio
        </Link>
      </li>
      <li>
        <Link {...scrollAtts} to="contact">
          Contact me
        </Link>
      </li>
      <li>
        <DarkModeToggle />
      </li>
    </ul>
  );
}

export default WebTabletHeader;
