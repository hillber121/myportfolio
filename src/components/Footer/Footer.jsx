import { useContext } from "react";
import { Link } from "react-scroll";

import "./_footer.scss";
import { AppContext } from "../store/AppProvider";
import { LinkedInIcon, TelegramIcon, GitlabIcon } from "./../SvgFile/SvgFile";

const socialLinks = [
  {
    name: "LinkedIn contact",
    icon: <LinkedInIcon />,
    url: "www.linkedin.com/in/dai-hung-nguyen",
  },
  {
    name: "Telegram contact",
    icon: <TelegramIcon />,
    url: "https://t.me/hillber121",
  },
  {
    name: "Gitlab contact",
    icon: <GitlabIcon />,
    url: "https://gitlab.com/hillber121",
  },
];
function Footer() {
  const scrollAtts = useContext(AppContext).scrollAtts;

  // Click button to open link handle
  const linksComp = socialLinks.map((socialLink) => (
    <a
      key={socialLink.name}
      href={socialLink.url}
      title={socialLink.url}
      target="_blank"
      rel="noopener noreferrer"
    >
      {socialLink.icon}
    </a>
  ));

  return (
    <div className="Footer">
      <div className="wide row">
        <div className="Footer-container">
          <div className="Footer__nav text-body-title">
            <Link {...scrollAtts} offset={-500} to="home">
              Intro
            </Link>
            <Link {...scrollAtts} to="portfolio">
              Portfolio
            </Link>
          </div>
          <div className="Footer__intro">
            <div className="text-footer-name">Nguyen Dai Hung</div>
            <div className="text-footer-title">Front-end developer</div>
          </div>
          <div className="Footer__contact">{linksComp}</div>
        </div>
        <div className="Footer__powered">Powered by Hung</div>
      </div>
    </div>
  );
}
export default Footer;
