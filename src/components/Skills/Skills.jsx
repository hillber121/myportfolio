import { useContext, useState } from "react";
import { Link } from "react-scroll";
import { SlideDown } from "react-slidedown";
import "react-slidedown/lib/slidedown.css";

import "./_skills.scss";
import { AppContext } from "../store/AppProvider";
import {
  NavLIcon,
  SoftSkillsIcon,
  TechSkillsIcon,
  HtmlIcon,
  CssIcon,
  SassIcon,
  JsIcon,
  ReactIcon,
  FigmaIcon,
  AttitudeIcon,
  CommuIcon,
  OrganIcon,
  ProblemIcon,
  TeamworkIcon,
  EnglishIcon,
} from "./../SvgFile/SvgFile";

function Skills() {
  const appCtx = useContext(AppContext);

  // Handle switch skills criteria
  const [activedSkill, setActivedSkill] = useState("tech");
  const techClickedHandler = () => {
    if (activedSkill === "tech") {
      // Toggle for mobile only
      if (!appCtx.isMobile) {
        return;
      }
      setActivedSkill("");
      return;
    }
    setActivedSkill("tech");
  };
  const softClickedHandler = () => {
    if (activedSkill === "soft") {
      // Toggle for mobile only
      if (!appCtx.isMobile) {
        return;
      }
      setActivedSkill("");
      return;
    }
    setActivedSkill("soft");
  };

  //   Skills component
  const techSkills = (
    <div className="Skills__tech">
      {/* HTML */}
      <div className="Skills__tech-tab">
        <div className="Skills__tech-tab-content">
          <div className="Skills__tech-tab-content-skill">
            <HtmlIcon />
            <span>Html</span>
          </div>
          <div className="Skills__tech-tab-content-percentage">80%</div>
        </div>
        <div className="Skills__tech-tab-bar">
          <div className="Skills__tech-tab-bar-html" />
        </div>
      </div>
      {/* JS */}
      <div className="Skills__tech-tab">
        <div className="Skills__tech-tab-content">
          <div className="Skills__tech-tab-content-skill">
            <JsIcon />
            <span>JavaScript</span>
          </div>
          <div className="Skills__tech-tab-content-percentage">70%</div>
        </div>
        <div className="Skills__tech-tab-bar">
          <div className="Skills__tech-tab-bar-js" />
        </div>
      </div>
      {/* CSS */}
      <div className="Skills__tech-tab">
        <div className="Skills__tech-tab-content">
          <div className="Skills__tech-tab-content-skill">
            <CssIcon />
            <span>Css</span>
          </div>
          <div className="Skills__tech-tab-content-percentage">80%</div>
        </div>
        <div className="Skills__tech-tab-bar">
          <div className="Skills__tech-tab-bar-css" />
        </div>
      </div>
      {/* React */}
      <div className="Skills__tech-tab">
        <div className="Skills__tech-tab-content">
          <div className="Skills__tech-tab-content-skill">
            <ReactIcon />
            <span>ReactJs</span>
          </div>
          <div className="Skills__tech-tab-content-percentage">70%</div>
        </div>
        <div className="Skills__tech-tab-bar">
          <div className="Skills__tech-tab-bar-react" />
        </div>
      </div>
      {/* SASS/SCSS */}
      <div className="Skills__tech-tab">
        <div className="Skills__tech-tab-content">
          <div className="Skills__tech-tab-content-skill">
            <SassIcon />
            <span>Sass/Scss</span>
          </div>
          <div className="Skills__tech-tab-content-percentage">60%</div>
        </div>
        <div className="Skills__tech-tab-bar">
          <div className="Skills__tech-tab-bar-sass" />
        </div>
      </div>
      {/* Figma */}
      <div className="Skills__tech-tab">
        <div className="Skills__tech-tab-content">
          <div className="Skills__tech-tab-content-skill">
            <FigmaIcon />
            <span>Figma</span>
          </div>
          <div className="Skills__tech-tab-content-percentage">60%</div>
        </div>
        <div className="Skills__tech-tab-bar">
          <div className="Skills__tech-tab-bar-figma" />
        </div>
      </div>
    </div>
  );
  const softSkills = (
    <div className="Skills__soft">
      <div className="Skills__soft-tab">
        <ProblemIcon />
        <span>Problem solving</span>
      </div>
      <div className="Skills__soft-tab">
        <AttitudeIcon />
        <span>Attitude</span>
      </div>
      <div className="Skills__soft-tab">
        <OrganIcon />
        <span>Organising</span>
      </div>
      <div className="Skills__soft-tab">
        <CommuIcon />
        <span>Communicating</span>
      </div>
      <div className="Skills__soft-tab">
        <TeamworkIcon />
        <span>Teamworking</span>
      </div>
      <div className="Skills__soft-tab">
        <EnglishIcon />
        <span>English (intermediate level)</span>
      </div>
    </div>
  );

  return (
    <section className="Skills" id="skills">
      <div className="section row">
        <div className="text-section-title">Skills</div>
        <div className="text-section-subtitle">Personal skills</div>
      </div>
      <div className="row">
        <div className="Skills__btns">
          <Link {...appCtx.scrollAtts} spy={false} to="skills">
            <div
              className={`Skills__btns-btn ${
                activedSkill === "tech" ? "Skills__btns-btn--active" : ""
              }`}
              onClick={techClickedHandler}
            >
              <TechSkillsIcon className="Skills__btns-btn-icon" />
              <div className="Skills__btns-btn-text">Technical skills</div>
              {appCtx.isMobile && (
                <NavLIcon className="Skills__btns-btn-navicon" />
              )}
            </div>
          </Link>
          {appCtx.isMobile && (
            <SlideDown
              style={{ transitionDuration: "0.3s" }}
              closed={activedSkill !== "tech"}
            >
              {techSkills}
            </SlideDown>
          )}
          <Link {...appCtx.scrollAtts} spy={false} to="skills">
            <div
              className={`Skills__btns-btn ${
                activedSkill === "soft" ? "Skills__btns-btn--active" : ""
              }`}
              onClick={softClickedHandler}
            >
              <SoftSkillsIcon className="Skills__btns-btn-icon" />
              <div className="Skills__btns-btn-text">Soft skills</div>
              {appCtx.isMobile && (
                <NavLIcon className="Skills__btns-btn-navicon" />
              )}
            </div>
          </Link>
          {appCtx.isMobile && (
            <SlideDown
              style={{ transitionDuration: "0.3s" }}
              closed={activedSkill !== "soft"}
            >
              {softSkills}
            </SlideDown>
          )}
        </div>
      </div>
      {!appCtx.isMobile && (
        <div className="row">
          {activedSkill === "tech"
            ? techSkills
            : activedSkill === "soft"
            ? softSkills
            : ""}
        </div>
      )}
    </section>
  );
}

export default Skills;
