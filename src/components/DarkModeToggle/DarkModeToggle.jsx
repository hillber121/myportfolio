import { useState, useEffect } from "react";

import Button from "../UI/Button";
import { MoonIcon, SunIcon } from "../SvgFile/SvgFile";

function DarkModeToggle() {
  const [darkTheme, setDarkTheme] = useState(sessionStorage.getItem("theme"));
  useEffect(() => {
    document
      .getElementsByTagName("html")[0]
      .setAttribute("data-theme", sessionStorage.getItem("theme"));
  }, []);

  const toggleDarkHandler = () => {
    if (darkTheme) {
      sessionStorage.removeItem("theme");
      document.getElementsByTagName("HTML")[0].removeAttribute("data-theme");
      setDarkTheme(false);
    } else {
      sessionStorage.setItem("theme", "dark");
      document
        .getElementsByTagName("HTML")[0]
        .setAttribute("data-theme", "dark");
      setDarkTheme(true);
    }
  };

  return (
    <Button
      type="checkbox"
      defaultChecked={darkTheme}
      isSmall
      icon={darkTheme ? <SunIcon /> : <MoonIcon />}
      onClick={toggleDarkHandler}
    >
      {darkTheme ? "Light" : "Dark"}
    </Button>
  );
}

export default DarkModeToggle;
